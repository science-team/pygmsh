Source: pygmsh
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Drew Parsons <dparsons@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-python,
 pybuild-plugin-pyproject, flit,
 python3-setuptools,
 python3-all:any,
 python3-gmsh, gmsh,
 python3-meshio (>= 4.3.2~),
 python3-numpy (>= 1.20.0~),
 python3-setuptools (>= 42~),
 python3-sphinx,
 python3-wheel
Build-Depends-Indep: libjs-mathjax,
 python3-sphinx-autodoc-typehints,
Standards-Version: 4.7.0
Homepage: https://github.com/nschloe/pygmsh
Vcs-Browser: https://salsa.debian.org/science-team/pygmsh
Vcs-Git: https://salsa.debian.org/science-team/pygmsh.git
Testsuite: autopkgtest-pkg-python

Package: python3-pygmsh
Architecture: all
Depends: python3-gmsh, gmsh,
 ${python3:Depends}, ${misc:Depends}
Suggests: python-pygmsh-doc
Description: combine the power of Gmsh with the versatility of Python
 Gmsh is a powerful mesh generation tool with a scripting language
 that is notoriously hard to write.
 .
 The goal of pygmsh is to combine the power of Gmsh with the
 versatility of Python and to provide useful abstractions from the
 Gmsh scripting language so you can create complex geometries more
 easily.
 .
 This package installs the library for Python 3.

Package: python-pygmsh-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: libjs-mathjax, ${sphinxdoc:Depends}, ${misc:Depends}
Description: combine the power of Gmsh with the versatility of Python (docs)
 Gmsh is a powerful mesh generation tool with a scripting language
 that is notoriously hard to write.
 .
 The goal of pygmsh is to combine the power of Gmsh with the
 versatility of Python and to provide useful abstractions from the
 Gmsh scripting language so you can create complex geometries more
 easily.
 .
 This is the common documentation package.
